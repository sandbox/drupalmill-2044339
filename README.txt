
CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Features
 * Installation

INTRODUCTION
------------
Sitebase is a pure Drupal 7 Core based install profile. It will not include any
contributed modules or themes.

FEATURES
--------
 * Creates administrator role and enables all permissions
 * Sets create new accounts to admin only
 * Enables content viewing for authorized users only
 * Stark as default theme
 * Seven as admin theme with Overlay

INSTALLATION
------------
Simply upload the folder to the /profiles folder of your Drupal docroot before
you run the install-script. Then select Sitebase as install profile.
